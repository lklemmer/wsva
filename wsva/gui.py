def gui_run(args):
    import sys
    from PySide6.QtWidgets import QApplication
    from wsva.mainwindow import MainWindow

    app = QApplication(sys.argv)

    window = MainWindow(args)
    window.show()

    sys.exit(app.exec())
