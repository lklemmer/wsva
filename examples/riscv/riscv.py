'''Python helper function for decoding RISC-V instructions'''
from riscvmodel import code
from riscvmodel.variant import Variant

variant = Variant('RV32G')

def decode_op(instr):
    '''Decodes a RISC-V instruction and returns a string description'''
    try:
        return str(code.decode(instr, variant)).split()[0]
    except Exception:
        return 'Invalid: ' + str(instr)
