PYTHON=python3
UI_FILES = $(wildcard ui/*.ui)
PY_FILES = $(patsubst %.ui,wsva/%.py,$(UI_FILES))

all: $(PY_FILES)

wsva/ui/%.py: ui/%.ui
	pyside6-uic $< -o $@

lint:
	ruff check wsva/ --exclude wsva/ui