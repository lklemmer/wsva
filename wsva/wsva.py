import argparse

from wsva.svac import compiler_run
from wsva.wsvacl import cl_run
from wsva.gui import gui_run


def main():
    parser = argparse.ArgumentParser(
        prog="Wal System Verilog Assertions",
        description="Check SVA Properties on Waveforms",
    )

    subparsers = parser.add_subparsers(help="sub-command help")
    subparsers.required = True

    # command line interface
    parser_cl = subparsers.add_parser("run", help="run SVA files on a waveform")
    parser_cl.add_argument("trace", help="path to waveform")
    parser_cl.add_argument("--sv", nargs="*", help="paths to SystemVerilog files")
    parser_cl.add_argument(
        "-k",
        "--keep-running",
        help="Keep running after first failing assertion",
        action="store_true",
    )
    parser_cl.add_argument(
        "-s", "--show", help="Open GTKWave on failing assertion", action="store_true"
    )
    parser_cl.set_defaults(func=cl_run)

    # compiler interface
    parser_compiler = subparsers.add_parser("compile", help="compile SVA file to WAL")
    parser_compiler.add_argument("sva_file", help="path to wal/wo file")
    parser_compiler.add_argument("-o", "--outfile", help="paths to waveforms to load")
    parser_compiler.set_defaults(func=compiler_run)

    # gui interface
    parser_gui = subparsers.add_parser("gui", help="open WSVA GUI")
    parser_gui.add_argument("trace", nargs="?", help="path to waveform")
    parser_gui.add_argument("--sv", help="path to SystemVerilog file")
    parser_gui.set_defaults(func=gui_run)

    args = parser.parse_args()
    args.func(args)
