import sys
import time
from multiprocessing import Pool, cpu_count, Manager

from wsva.parser import parse_sva, assertions_to_wal_functions
from wal.core import Wal
from wal.ast_defs import Operator as Op
from wsva.waveformviewer import open_gtkwave
from wsva.types import SingleTimeFunction


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


frames = ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"]
interval = 80


def show_failing_assertion(wal, status, assertions, trace):
    failing = []
    for label in status:
        if status[label]["fails"]:
            failing.append(label)

    if failing:
        labels = {
            "Q": "Quit WSVA",
            "q": "Quit WSVA",
            "quit": "Quit WSVA",
            "exit": "Quit WSVA",
        }

        print(f"{bcolors.FAIL}{bcolors.UNDERLINE}Failing Properties{bcolors.ENDC}")
        for num, property in enumerate(failing, start=1):
            labels[str(num)] = property
            print(f"{num:3d}: {property}")

        print("  q: Quit WSVA")

        selected = input(
            f"{bcolors.WARNING}Enter number of failing assertion to open in waveform viewer {bcolors.ENDC}:\n"
        )
        label = labels[selected]
    else:
        return

    if label == "Quit WSVA":
        return

    signals = wal.eval_str("SIGNALS")
    for assertion in assertions:
        if assertion.label.name == label:
            signals = assertion.get_signals()
            break

    time = status[label]["fails"][0]

    open_gtkwave(wal, trace, label, signals, time)


def check_property(data):
    function = data["function"]
    prologue = data["prologue"]
    clocking_groups = data["clocking"]
    trace = data["trace"]
    args = data["args"]
    queue = data["queue"]

    wal = Wal()
    wal.eval_context.global_environment.write("args", [])
    wal.load(trace)
    # evaluate prologue
    for stmt in prologue:
        wal.eval(stmt)

    for clock_group in clocking_groups:
        wal.eval([Op.DEFINE, clock_group[0], clock_group[1]])

    wal.eval(function)
    label = function[1]
    res = wal.eval([label], stop_on_fail=not args.keep_running)
    queue.put(label)

    return {"label": label.name, "fails": res}


def cl_run(args):
    sys.setrecursionlimit(10000)

    signals = []
    assertions = {}

    wal = Wal()
    if args.show:
        wal.load(args.trace)
        signals = wal.eval_str("SIGNALS")

    for svfile in args.sv:
        status = {}
        # parse sva
        with open(svfile) as svaf:
            trace = args.trace
            print(
                f"{bcolors.WARNING}{bcolors.BOLD}Trace:{bcolors.ENDC}{bcolors.WARNING} {trace}{bcolors.ENDC}"
            )  # noqa: E501
            print(
                f"{bcolors.WARNING}{bcolors.BOLD}SVA  :{bcolors.ENDC}{bcolors.WARNING} {svfile}{bcolors.ENDC}"
            )  # noqa: E501

            code_txt = svaf.read()
            parsed = parse_sva(code_txt, signals=signals)
            prologue = parsed["prologue"]
            clocking_groups = [
                [grp[0], grp[1]] for grp in parsed["clocking_groups"].values()
            ]

            manager = Manager()
            result_queue = manager.Queue()
            data = []

            assertions = assertions_to_wal_functions(parsed["assertions"])

            # find single_time_functions
            single_time_functions = {}
            for function in assertions:
                if isinstance(function[1], SingleTimeFunction):
                    single_time_functions[function[1].name] = function

            for function in assertions:
                # skip the single time functions
                if isinstance(function[1], SingleTimeFunction):
                    continue

                single_time_function = single_time_functions[
                    "sva_" + function[1].name + "_single"
                ]
                data.append(
                    {
                        "function": function,
                        "prologue": prologue
                        + [single_time_function],  # plug in the correct stf
                        "clocking": clocking_groups,
                        "trace": trace,
                        "args": args,
                        "queue": result_queue,
                    }
                )

            status = {}
            n_properties = len(data)

            with Pool(cpu_count() - 1) as p:
                res = p.map_async(check_property, data)

                try:
                    print("\033[?25l", end="")
                    while not res.ready():
                        for frame in frames:
                            sys.stdout.write(
                                f"\r{bcolors.BOLD}{bcolors.OKCYAN}Checking Properties {result_queue.qsize()}/{n_properties} {frame}{bcolors.ENDC}\033[K"
                            )  # noqa: E501
                            sys.stdout.flush()
                            time.sleep(0.001 * 80)
                finally:
                    print(
                        "\r\r                                                ", end=""
                    )  # noqa: E501
                    print("\r\r\033[?25h", end="")

            for property_res in res.get():
                status[property_res["label"]] = property_res

        label_width = 20
        if list(status.keys()):
            label_width = max(map(len, list(status.keys())))

        lines = []
        for assertionid, data in status.items():
            if not data["fails"]:
                lines.append(
                    f"{assertionid:{label_width}} : {bcolors.BOLD}{bcolors.OKGREEN}PASS{bcolors.ENDC}"
                )  # noqa: E501
            else:
                fails = " ".join(map(str, map(lambda xs: xs[1], data["fails"][:10])))
                if len(data["fails"]) > len(fails):
                    fails = fails + " ..."
                lines.append(
                    f"{assertionid:{label_width}} : {bcolors.BOLD}{bcolors.FAIL}FAIL{bcolors.ENDC} @ {fails}"
                )  # noqa: E501

        line_width = 20
        if lines:
            line_width = max(map(len, lines))

        start = f'{bcolors.UNDERLINE}{bcolors.BOLD}{bcolors.HEADER}{"Property":{label_width}}   Result'  # noqa: E501
        print(f"{start:{line_width}}{bcolors.ENDC}")
        for line in lines:
            print(line)
        print()

        if args.show:
            show_failing_assertion(wal, status, parsed["assertions"], trace)
