from wal.ast_defs import Symbol
from wal.ast_defs import Symbol as S
from wal.ast_defs import Operator as Op
from wal.ast_defs import WList


def re(x, o):
    return [Op.REL_EVAL, x, o]


class Emitting:
    def emit(self, assertion):
        pass


class SingleTimeFunction(Symbol):
    def __init__(self, name):
        super().__init__(name)


class Local(Symbol):
    def __init__(self, name, initial_value):
        super().__init__(name)
        self.initial_value = initial_value
        self.data_type = None


class Assertion(Emitting):
    def __init__(self, signals):
        self.label = "prop"
        self.clocking = None
        self.clocking_expr = None
        self.reset_expr = None
        self.expr = True
        self.locals = []
        self.single_time = None
        self.found_signals = set()
        self.stop_on_fail = True
        self.signals = signals
        self.declaration = False

    def get_signals(self):
        def find_signals(expr):
            if isinstance(expr, Symbol):
                self.found_signals.add(expr.name)
            elif isinstance(expr, (WList, list)):
                for x in expr:
                    find_signals(x)
            else:
                return

        find_signals(self.single_time)
        find_signals(self.clocking_expr)
        find_signals(self.reset_expr)
        self.found_signals = set([x for x in self.found_signals if x in self.signals])
        return self.found_signals

    def emit_internal(self, expr):
        if isinstance(expr, Emitting):
            return (True, expr.emit(self))
        elif isinstance(expr, (WList, list)):
            res = []
            keep_going = False
            for x in expr:
                work, emitted = self.emit_internal(x)
                res.append(emitted)
                if work:
                    keep_going = True

            return (keep_going, res)
        else:
            return (False, expr)

    def emit(self):
        single_time_label = SingleTimeFunction("sva_" + self.label.name + "_single")
        single_time = [
            S("defun"),
            single_time_label,
            [],
            [Op.DEFINE, S("was-disabled?"), False],
            [Op.DEFINE, S("started-at"), S("TS")],
            [Op.DEFINE, S("property-status"), False],
        ] + [[Op.DEFINE, local, local.initial_value] for local in self.locals] + self.expr

        if self.clocking:
            body = [
                [Op.DEFINE, S("keep-running"), True],
                [Op.DEFINE, S("fails"), [Op.LIST]],
                [Op.DEFINE, S("result"), False],
                [Op.SAMPLE_AT, self.clocking],
                [
                    Op.WHILE,
                    S("keep-running"),
                    [Op.SET, [S("result"), [single_time_label]]],
                    [
                        S("when"),
                        S("result"),
                        [
                            Op.SET,
                            [S("fails"), [S("append"), S("fails"), S("result")]],
                            [S("keep-running"), [Op.NOT, S("stop_on_fail")]],
                        ],
                    ],
                    [
                        Op.SET,
                        [S("keep-running"), [Op.AND, S("keep-running"), [Op.STEP]]],
                    ],
                ],
                [Op.SAMPLE_AT, S("sva-clk-std")],
                S("fails"),
            ]
        else:
            body = [
                [Op.DEFINE, S("keep-running"), True],
                [Op.DEFINE, S("fails"), [Op.LIST]],
                [Op.DEFINE, S("result"), False],
                [
                    Op.WHILE,
                    S("keep-running"),
                    [Op.SET, [S("result"), [single_time_label]]],
                    [
                        S("when"),
                        S("result"),
                        [
                            Op.SET,
                            [S("fails"), [S("append"), S("fails"), S("result")]],
                            [S("keep-running"), [Op.NOT, S("stop_on_fail")]],
                        ],
                    ],
                    [
                        Op.SET,
                        [S("keep-running"), [Op.AND, S("keep-running"), [Op.STEP]]],
                    ],
                ],
                S("fails"),
            ]

        # fill holes until they are all filled
        keep_going = True
        while keep_going:
            keep_going, body = self.emit_internal(body)

        # fill holes until they are all filled
        keep_going = True
        while keep_going:
            keep_going, single_time = self.emit_internal(single_time)

        function = [S("defun"), self.label, []] + body

        self.expr = [f"Assertion: {self.label.name}", body]
        self.single_time = single_time
        return [single_time, function]


class Hole(Emitting):
    def __init__(self, cc):
        self.cc = cc

    def emit(self, assertion):
        return self.cc(assertion)


def inject_label(assertion):
    return assertion.label.name


def inject_found_signals(assertion):
    return assertion.found_signals


class Fail(Emitting):
    def __init__(self, wrapping=None, msg=""):
        self.wrapping = wrapping
        self.msg = msg

    def emit(self, assertion):
        code = [[Op.LIST, S("started-at"), S("TS")]]

        if self.wrapping:
            return self.wrapping + code

        return code


class Definition(Emitting):
    def __init__(self, label, expr):
        self.label = label
        self.expr = expr

    def emit(self):
        return [Op.DEFMACRO, self.label, [], [Op.QUOTE, self.expr]]
