`wal (import riscv)

// python files can be included using WAL's import function
// and used using the call function as shown below

check_addi: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (call(riscv.decode_op, testbench.dut.rvsingle.instr) == "addi") |-> (testbench.dut.rvsingle.aluresult == (signed(testbench.dut.rvsingle.dp.srca) + signed(testbench.dut.rvsingle.dp.srcb)))
);
