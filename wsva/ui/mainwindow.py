# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.6.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import QCoreApplication, QMetaObject, QRect
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QHBoxLayout, QMenu, QMenuBar, QWidget


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 600)
        self.actionOpen_Trace = QAction(MainWindow)
        self.actionOpen_Trace.setObjectName("actionOpen_Trace")
        self.actionOpen_SVA = QAction(MainWindow)
        self.actionOpen_SVA.setObjectName("actionOpen_SVA")
        self.actionExit = QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionRun_All = QAction(MainWindow)
        self.actionRun_All.setObjectName("actionRun_All")
        self.actionRun_Selected = QAction(MainWindow)
        self.actionRun_Selected.setObjectName("actionRun_Selected")
        self.actionSave = QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionSave_As = QAction(MainWindow)
        self.actionSave_As.setObjectName("actionSave_As")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.mainview = QWidget(self.centralwidget)
        self.mainview.setObjectName("mainview")

        self.horizontalLayout.addWidget(self.mainview)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName("menubar")
        self.menubar.setGeometry(QRect(0, 0, 1200, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuRun = QMenu(self.menubar)
        self.menuRun.setObjectName("menuRun")
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuRun.menuAction())
        self.menuFile.addAction(self.actionOpen_Trace)
        self.menuFile.addAction(self.actionOpen_SVA)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionSave_As)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuRun.addAction(self.actionRun_All)
        self.menuRun.addAction(self.actionRun_Selected)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QCoreApplication.translate("MainWindow", "SVA IDE", None)
        )
        self.actionOpen_Trace.setText(
            QCoreApplication.translate("MainWindow", "Open Trace File", None)
        )
        self.actionOpen_SVA.setText(
            QCoreApplication.translate("MainWindow", "O&pen SVA File", None)
        )
        # if QT_CONFIG(shortcut)
        self.actionOpen_SVA.setShortcut(
            QCoreApplication.translate("MainWindow", "Ctrl+O", None)
        )
        # endif // QT_CONFIG(shortcut)
        self.actionExit.setText(QCoreApplication.translate("MainWindow", "Exit", None))
        self.actionRun_All.setText(
            QCoreApplication.translate("MainWindow", "All", None)
        )
        # if QT_CONFIG(tooltip)
        self.actionRun_All.setToolTip(
            QCoreApplication.translate("MainWindow", "Run All Properties", None)
        )
        # endif // QT_CONFIG(tooltip)
        self.actionRun_Selected.setText(
            QCoreApplication.translate("MainWindow", "Selected", None)
        )
        # if QT_CONFIG(tooltip)
        self.actionRun_Selected.setToolTip(
            QCoreApplication.translate("MainWindow", "Run Selected Property", None)
        )
        # endif // QT_CONFIG(tooltip)
        self.actionSave.setText(QCoreApplication.translate("MainWindow", "S&ave", None))
        self.actionSave_As.setText(
            QCoreApplication.translate("MainWindow", "Save As...", None)
        )
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", "File", None))
        self.menuRun.setTitle(QCoreApplication.translate("MainWindow", "Run", None))

    # retranslateUi
