# WSVA - A SystemVerilog Assertion (SVA) to WAL compiler
WSVA is a currently highly experimental, SystemVerilog Assertion to [WAL](https://github.com/ics-jku/wal) compiler
that allows checking SVA properties on waveforms (vcd and fst).

**WSVA is an experimental proof of concept and not intended for verification.**

It provides three modes, a command line tool to check properties, a compiler that emits 
WAL code which can be used in other WAL programs, and a GUI to develop and run SVA properties.

## Getting Started
First, install [WAL](https://github.com/ics-jku/wal/).
WSVA can be installed after cloning this repository with `pip install.`.

To add support for _fst_ waveforms install _pylibfst_ with `pipenv install pylibfst` (currently not available on Windows).

## Start WSVA
To get help:
`wsva help`

Starting the GUI requires installing the _PySide6_ package with `pipenv install PySide6`.

To run the properties from _properties.sv_ on _trace.fst_ run:
`wsva run trace.fst --sv properties.sv`

To check multiple waveforms just add more files.
`wsva run trace.fst trace2.vcd --sv properties.sv`

To open a failed assumption in GTKWave.
`wsva run trace.fst --sv properties.sv --show`

To show all times at which an property fails.
`wsva run trace.fst --sv properties.sv --keep-running`

To compile the properties from _properties.sv_ to _properties.wal_
`wsva compile properties.sv`

To start the GUI
`wsva gui` 

## Supported SVA Features
Currently, WSVA supports the following SVA operators fully or partially.
Again, this is highly WIP and not intended for verification.

| Operator      | Supported |
| ---           | ---       |
| ##n a         | x         |
| a ##n b       | x         |
| a ##[l:h] b   | partial   |
| a [*n]        | partial   |
| a [->n]       | partial   |
| a \|-> b      | x         |
| a \|=> b      | x         |
| a && b        | x         |
| a \|\| b      | x         |
| a < b ...     | x         |
| $rose(a)      | x         |
| $fell(a)      | x         |
| $stable(a)    | x         |
| $past(a, o=1) | x         |

WSVA also supports following additional features:

| Feature                     | Supported |
|-----------------------------|-----------|
| property-endproperty syntax | x         |
| local variables             | partial   |

## Macros
WSVA supports a rudimentary macro system.

```
`define abc 5
`define def 1 + `abc

`define add(x) x + x
```

## WAL integration
Arbitrary WAL code can be executed before the SVA properties are executed using the ``wal` directive.

```
`wal (print "Starting")
`wal (eval-file my_wal_file)
```

All WAL functions can be called inside SVA expressions. To call a WAL function,
convert the typical WAL function call notation `(name arg1 arg2)` to the C-like `name(arg1, arg2)` notation.
Functions defined in other WAL files can be called the same way after importing them using the WAL directive and `eval-file`.

## Limitations and Future Work
- Experimental support only for most operators
- Limited local variable support
- No vacuous assertion results
- No cover statements
- no generate statements
- binding
- improved parser, possibly using [slang](https://sv-lang.com/)
