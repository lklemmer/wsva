`wal (define testvalue 1)

check_inc: assert property(
  @(posedge tb.clk)
  (tb.dut.value == ($past(tb.dut.value) + 1))
);

reset: assert property(
  @(posedge tb.clk)
  disable iff (tb.reset == 1 || $past(tb.reset) == 1)
  (tb.value == ($past(tb.value) + 1) )
);

reset_and_overflow: assert property(
  @(posedge tb.clk)
  disable iff (tb.reset == 1 || tb.value == 0)
  (tb.value == ($past(tb.value) + 1) )
);

test_consecutive_rep: assert property(
  @(posedge tb.clk)
  tb.dut.value |-> (tb.dut.value[*2])
);

test_non_consecutive_rep: assert property(
  @(posedge tb.clk)
  disable iff (tb.reset == 1)                                          
  !tb.dut.value |-> (tb.dut.value[->1] ##1 !tb.dut.value)
);
