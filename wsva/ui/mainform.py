# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainform.ui'
##
## Created by: Qt User Interface Compiler version 6.6.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (
    QCoreApplication,
    QDate,
    QDateTime,
    QLocale,
    QMetaObject,
    QObject,
    QPoint,
    QRect,
    QSize,
    QTime,
    QUrl,
    Qt,
)
from PySide6.QtGui import (
    QBrush,
    QColor,
    QConicalGradient,
    QCursor,
    QFont,
    QFontDatabase,
    QGradient,
    QIcon,
    QImage,
    QKeySequence,
    QLinearGradient,
    QPainter,
    QPalette,
    QPixmap,
    QRadialGradient,
    QTransform,
)
from PySide6.QtWidgets import (
    QAbstractItemView,
    QAbstractScrollArea,
    QApplication,
    QGridLayout,
    QHBoxLayout,
    QHeaderView,
    QLabel,
    QProgressBar,
    QSizePolicy,
    QSplitter,
    QTabWidget,
    QTableWidget,
    QTableWidgetItem,
    QTextEdit,
    QTreeWidget,
    QTreeWidgetItem,
    QVBoxLayout,
    QWidget,
)


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName("Form")
        Form.resize(1411, 779)
        self.gridLayout_2 = QGridLayout(Form)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.splitter = QSplitter(Form)
        self.splitter.setObjectName("splitter")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(54)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.splitter.sizePolicy().hasHeightForWidth())
        self.splitter.setSizePolicy(sizePolicy)
        self.splitter.setOrientation(Qt.Horizontal)
        self.verticalLayoutWidget = QWidget(self.splitter)
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout_4 = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_4.setSpacing(2)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.widget = QWidget(self.verticalLayoutWidget)
        self.widget.setObjectName("widget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy1)
        self.verticalLayout_7 = QVBoxLayout(self.widget)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.splitter_2 = QSplitter(self.widget)
        self.splitter_2.setObjectName("splitter_2")
        self.splitter_2.setOrientation(Qt.Vertical)
        self.traceStats = QTableWidget(self.splitter_2)
        if self.traceStats.columnCount() < 1:
            self.traceStats.setColumnCount(1)
        self.traceStats.setObjectName("traceStats")
        self.traceStats.setEnabled(True)
        self.traceStats.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.traceStats.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.traceStats.setTabKeyNavigation(False)
        self.traceStats.setProperty("showDropIndicator", True)
        self.traceStats.setRowCount(0)
        self.traceStats.setColumnCount(1)
        self.splitter_2.addWidget(self.traceStats)
        self.traceStats.horizontalHeader().setVisible(False)
        self.traceStats.horizontalHeader().setCascadingSectionResizes(False)
        self.traceStats.horizontalHeader().setStretchLastSection(True)
        self.traceStats.verticalHeader().setVisible(False)
        self.traceStats.verticalHeader().setStretchLastSection(False)
        self.designHierarchy = QTreeWidget(self.splitter_2)
        self.designHierarchy.setObjectName("designHierarchy")
        self.designHierarchy.setEnabled(True)
        self.designHierarchy.setAutoExpandDelay(-1)
        self.designHierarchy.setAnimated(False)
        self.splitter_2.addWidget(self.designHierarchy)
        self.designHierarchy.header().setVisible(True)

        self.verticalLayout_7.addWidget(self.splitter_2)

        self.verticalLayout_4.addWidget(self.widget)

        self.splitter.addWidget(self.verticalLayoutWidget)
        self.verticalLayoutWidget_2 = QWidget(self.splitter)
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_5 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.tabWidget = QTabWidget(self.verticalLayoutWidget_2)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName("tab")
        self.horizontalLayout = QHBoxLayout(self.tab)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.sva_editor = QTextEdit(self.tab)
        self.sva_editor.setObjectName("sva_editor")

        self.horizontalLayout.addWidget(self.sva_editor)

        self.tabWidget.addTab(self.tab, "")

        self.verticalLayout_5.addWidget(self.tabWidget)

        self.verticalLayout_5.setStretch(0, 1)
        self.splitter.addWidget(self.verticalLayoutWidget_2)
        self.verticalLayoutWidget_3 = QWidget(self.splitter)
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.verticalLayout_6 = QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_6.setSpacing(1)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.progressWidget = QWidget(self.verticalLayoutWidget_3)
        self.progressWidget.setObjectName("progressWidget")
        self.progressWidget.setEnabled(True)
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(
            self.progressWidget.sizePolicy().hasHeightForWidth()
        )
        self.progressWidget.setSizePolicy(sizePolicy2)
        self.progressWidget.setMinimumSize(QSize(0, 60))
        self.verticalLayoutWidget_4 = QWidget(self.progressWidget)
        self.verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")
        self.verticalLayoutWidget_4.setGeometry(QRect(0, 0, 437, 50))
        self.progressLayout = QVBoxLayout(self.verticalLayoutWidget_4)
        self.progressLayout.setObjectName("progressLayout")
        self.progressLayout.setContentsMargins(0, 0, 0, 0)
        self.progressLabel = QLabel(self.verticalLayoutWidget_4)
        self.progressLabel.setObjectName("progressLabel")

        self.progressLayout.addWidget(self.progressLabel)

        self.verificationProgress = QProgressBar(self.verticalLayoutWidget_4)
        self.verificationProgress.setObjectName("verificationProgress")
        self.verificationProgress.setValue(0)

        self.progressLayout.addWidget(self.verificationProgress)

        self.verticalLayout_6.addWidget(self.progressWidget)

        self.verificationResults = QTreeWidget(self.verticalLayoutWidget_3)
        self.verificationResults.setObjectName("verificationResults")

        self.verticalLayout_6.addWidget(self.verificationResults)

        self.splitter.addWidget(self.verticalLayoutWidget_3)

        self.gridLayout_2.addWidget(self.splitter, 0, 0, 1, 1)

        self.retranslateUi(Form)

        self.tabWidget.setCurrentIndex(0)

        QMetaObject.connectSlotsByName(Form)

    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", "Form", None))
        ___qtreewidgetitem = self.designHierarchy.headerItem()
        ___qtreewidgetitem.setText(
            0, QCoreApplication.translate("Form", "Design Hierarchy", None)
        )
        self.tabWidget.setTabText(
            self.tabWidget.indexOf(self.tab),
            QCoreApplication.translate("Form", "SVA", None),
        )
        self.progressLabel.setText(
            QCoreApplication.translate("Form", "Verification Progress", None)
        )
        ___qtreewidgetitem1 = self.verificationResults.headerItem()
        ___qtreewidgetitem1.setText(
            0, QCoreApplication.translate("Form", "Verification Results", None)
        )


# retranslateUi
