`define OP_LUI 'b0110111
`define OP_AUIPC 'b0010111
`define OP_ARITH 'b0010011
`define OP_LOAD 'b0000011
`define OP_STORE 'b0100011
`define OP_JAL 'b1101111

`define F7_BRANCH 'b1100011

`define F3_BEQ 'b000
`define F3_BNE 'b001
`define F3_BLTS 'b100
`define F3_BGES 'b101
`define F3_BLTU 'b110
`define F3_BGEU 'b111

// pc checks
pc_after_lui: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_LUI) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_auipc: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_AUIPC) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_arith: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_ARITH) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 54))
);

pc_after_load: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_LOAD) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 54))
);

pc_after_store: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_STORE) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_jal: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `OP_JAL) |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_taken_beq: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BEQ) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_beq: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BEQ) 
  && (testbench.dut.rvsingle.dp.srca != testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_taken_bne: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BNE) 
  && (testbench.dut.rvsingle.dp.srca != testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_bne: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BNE) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_taken_blt: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BLTS) 
  && ($signed(testbench.dut.rvsingle.dp.srca) < $signed(testbench.dut.rvsingle.dp.srcb))
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_blt: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BLTS) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  && ($signed(testbench.dut.rvsingle.dp.srca) >= $signed(testbench.dut.rvsingle.dp.srcb))
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_taken_bge: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BGES) 
  && ($signed(testbench.dut.rvsingle.dp.srca) >= $signed(testbench.dut.rvsingle.dp.srcb))
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_bge: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BGES) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  && ($signed(testbench.dut.rvsingle.dp.srca) < $signed(testbench.dut.rvsingle.dp.srcb))
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_taken_bltu: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BLTU) 
  && (testbench.dut.rvsingle.dp.srca < testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_bge: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BLTU) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  && (testbench.dut.rvsingle.dp.srca < testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);

pc_after_taken_bgeu: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BGEU) 
  && (testbench.dut.rvsingle.dp.srca >= testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + $past($signed(testbench.dut.rvsingle.dp.immext))))
);

pc_after_not_taken_bge: assert property(
  @(posedge testbench.clk)
  disable iff(testbench.reset)
  (testbench.dut.rvsingle.instr[6:0] == `F7_BRANCH) 
  && (testbench.dut.rvsingle.instr[14:12] == `F3_BGEU) 
  && (testbench.dut.rvsingle.dp.srca == testbench.dut.rvsingle.dp.srcb)
  && (testbench.dut.rvsingle.dp.srca < testbench.dut.rvsingle.dp.srcb)
  |=> (testbench.dut.rvsingle.pc == ($past(testbench.dut.rvsingle.pc) + 4))
);
