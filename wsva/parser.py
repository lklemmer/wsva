"""Parsers for WAWK"""

# ruff: noqa: E731
import ast

from lark import Lark, Transformer, Token
from lark.exceptions import ParseError
from wal.ast_defs import Symbol as S
from wal.ast_defs import Operator as Op
from wal.reader import read_wal_sexpr
from wal.util import wal_str
from wsva.types import Assertion, Definition, Fail, Local, re
from wsva.preprocessor import preprocess

operators = [op.value for op in Op]

"""
    ?expr: symbol
         | "(" expr ")"
         | neg
         | imp_s
         | sum_s
         | or_s
         | sva_sequence
         | int
         | string
"""

WSVA_GRAMMAR = r"""

    _NL: /(\r?\n)+/
    _COMMENT: _WS? "//" /.*/
    _WS: WS
    _INTER : _COMMENT | _WS
    stmt : assertion | definition
    definition : "`define" base_symbol "=" int
    assertion : base_symbol ":" "assert" "property" "(" (property_spec) ")" ";"
    assertions : (assertion | property_declaration)+

    property_declaration : "property" base_symbol ";" (assertion_variable_declaration)* property_spec [";"] "endproperty" [":" base_symbol]
        
    assertion_variable_declaration : data_type (variable_decl_assignment)+ ";"

    !data_type : "int" | "logic"
    variable_decl_assignment : [","] base_symbol ["=" INT]

    property_spec : [clocking_event] [disable_iff] expr
    clocking_event : "@" "(" edge_identifier ")"
    disable_iff : "disable" "iff" "(" expr ")"

    edge_identifier : posedge | negedge | edge
    posedge : "posedge" expr
    negedge : "negedge" expr
    edge : "edge" expr

    ?expr: implication | sva_sequence

    symbol : base_symbol | scoped_symbol | grouped_symbol | timed_symbol 
        | bit_symbol | sliced_symbol | definition_symbol | sva_op
    scoped_symbol : "~" base_symbol
    grouped_symbol : "#" base_symbol
    definition_symbol : "`" base_symbol
    timed_symbol : base_symbol "@" SIGNED_INT
    !base_symbol : (LETTER | "_" | "." | "$") (LETTER | INT | "_" | "$" | ".")*
    bit_symbol : base_symbol "[" INT "]"
    sliced_symbol : base_symbol "[" INT ":" INT "]"

    int : INT | SIGNED_INT | dec_int | hex_int | bin_int
    dec_int : /'d[0-9]+/
    hex_int : /'h[0-9a-fA-F]+/
    bin_int : /'b[0-1]+/

    string : ESCAPED_STRING

    ?implication: or
                | implication implication_op or

    ?or: and
       | or or_op and -> bin_op

    ?and: comparison
        | and and_op comparison -> bin_op

    ?comparison: equality
               | comparison comparison_op equality -> bin_op

    ?equality: sum
             | equality equality_op sum -> bin_op

    ?sum: product
        | sum sum_op product -> bin_op

    ?product: negation
        | product product_op negation -> bin_op

    ?negation : [negation_op] atom

    atom: int
         | symbol
         | string
         | "(" expr ")"

    !negation_op : "!" | "not"
    !product_op : "*" | "/"
    !sum_op : "+" | "-"
    !comparison_op : ">" | "<" | ">=" | "<="
    !equality_op: "==" | "!="
    !and_op : "&&"
    !or_op : "||"
    !implication_op : "|->" | "|=>"
    !assign_op : "="

    sva_sequence : seq_delay_un | seq_delay_single | seq_repetition_once 
        | seq_delay_range | seq_goto_once | seq_var_assignment
    seq_delay_un : "##" INT expr
    seq_delay_single : expr "##" INT expr
    seq_delay_range : expr "##[" INT ":" INT "]" expr
    seq_repetition_once : expr "[*" INT "]"
    seq_goto_once : expr "[->" INT "]"
    seq_var_assignment : expr ("," sva_var_assignment)+
    
    sva_var_assignment : base_symbol assign_op expr

    sva_op : op_rose | op_fell | op_stable | op_past | op_signed | op_wal
    op_rose : "$rose(" expr ")"
    op_fell : "$fell(" expr ")"
    op_stable : "$stable(" expr ")"
    op_past : "$past(" expr ["," expr] ")"
    op_signed : "$signed(" expr ")"
    op_wal : base_symbol "(" [expr ("," expr)*] ")"

    %import common.ESCAPED_STRING
    %import common.SIGNED_INT
    %import common.INT
    %import common.WORD
    %import common.WS_INLINE
    %import common.WS
    %import common.LETTER
    %import common.NEWLINE
    %ignore WS
    """


class TreeToWal(Transformer):
    """Transforms a parsed tree into WAL data structures"""

    bin_op = lambda self, s: [Op(s[1]), s[0], s[2]]
    _ = lambda self, s: s[0]
    sum_op = lambda self, s: s[0]
    assign_op = lambda self, s: s[0]
    product_op = lambda self, s: s[0]
    equality_op = lambda self, s: s[0] if s[0] == "!=" else "="
    negation_op = lambda self, s: Op.NOT
    comparison_op = lambda self, s: s[0]
    operator = lambda self, s: s[0]
    atom = lambda self, s: s[0]
    neg = lambda self, s: s

    number = lambda self, n: int(n[0])
    simple_symbol = lambda self, s: s[0]
    symbol = lambda self, s: s[0]
    base_symbol = lambda self, s: S(
        "".join(list(map(lambda x: x.value if isinstance(x, Token) else str(x), s)))
    )
    data_type = lambda self, s: s[0]

    timed_symbol = lambda self, s: [Op.REL_EVAL, s[0], self.number([s[1]])]
    bit_symbol = lambda self, s: [Op.SLICE, s[0], self.number([s[1]])]
    sliced_symbol = lambda self, s: [
        Op.SLICE,
        s[0],
        self.number([s[1]]),
        self.number([s[2]]),
    ]
    definition_symbol = lambda self, s: [s[0]]

    def __init__(self, signals):
        self.signals = signals
        self.clocking_groups = {True: [S("sva-clk-std"), [Op.FIND, True]]}
        self.clocking_id = 0
        self.declarations = {}

    def next_clocking_id(self):
        self.clocking_id = self.clocking_id + 1
        return self.clocking_id

    def definition(self, s):
        return Definition(s[0], s[1])

    def assertions(self, s):
        return [assertion for assertion in s if assertion is not None]

    def assertion(self, s):
        assertion = s[1]
        assertion.label = s[0]
        return assertion

    def property_declaration(self, s):
        assertion = s[-2]
        assertion.locals = [local for local_list in s[1:-2] for local in local_list]
        self.declarations[s[0].name] = assertion
        return

    def assertion_variable_declaration(self, s):
        for local in s[1:]:
            local.data_type = str(s[0])
            if not local.initial_value:
                local.initial_value = 0
            if local.data_type == "logic":
                local.initial_value = bool(local.initial_value)
        return s[1:]

    def variable_decl_assignment(self, s):
        local = Local(s[0].name, s[1])
        return local

    def property_spec(self, s):
        assertion = Assertion(self.signals)

        def handle_clocking(clocking_event):
            clocking_key = wal_str(clocking_event)
            if clocking_key in self.clocking_groups:
                clocking_id = self.clocking_groups[clocking_key][0]
            else:
                clocking_id = S(f"sva-clk-{self.next_clocking_id()}")
                self.clocking_groups[clocking_key] = [
                    clocking_id,
                    [Op.FIND, [Op.REL_EVAL, clocking_event, 1]],
                ]

            return clocking_id

        disable_iff = [S("defun"), S("check-disable"), [], False]
        clock_expr = s[0]
        disable_expr = s[1]

        prop_expr = s[2]
        if disable_expr:
            prop_expr = [Op.DO, [S("check-disable")], s[2]]

        if not clock_expr and not disable_expr:  # no clocking and no disable
            if isinstance(prop_expr, S) and self.declarations.keys().__contains__(prop_expr.name):
                # must be use of a property declaration
                assertion = self.declarations[prop_expr.name]
                disable_iff = None
            else:
                assertion.expr = [[S("timeframe"), Fail(wrapping=[S("unless"), prop_expr])]]
                assertion.reset_expr = False
        elif clock_expr and not disable_expr:  # only clocking
            clock_expr = clock_expr[1]
            clocking_id = handle_clocking(clock_expr)
            assertion.clocking = clocking_id
            assertion.clocking_expr = clock_expr
            assertion.expr = [[S("timeframe"), Fail(wrapping=[S("unless"), prop_expr])]]
        if not clock_expr and disable_expr:  # only disable
            disable_expr = disable_expr[1]
            assertion.reset_expr = disable_expr
            disable_iff = [
                S("defun"),
                S("check-disable"),
                [],
                [
                    S("unless"),
                    S("was-disabled?"),
                    [Op.SET, [S("was-disabled?"), assertion.reset_expr]],
                ],
            ]
            prop_check = [Op.SET, [S("property-status"), prop_expr]]
            assertion.expr = [
                [
                    S("timeframe"),
                    prop_check,
                    Fail(
                        wrapping=[
                            S("unless"),
                            [Op.OR, S("property-status"), S("was-disabled?")],
                        ]
                    ),
                ]
            ]
        elif clock_expr and disable_expr:  # clocking and disable
            disable_expr = disable_expr[1]
            clock_expr = clock_expr[1]
            clocking_id = handle_clocking(clock_expr)
            assertion.clocking = clocking_id
            assertion.clocking_expr = clock_expr
            assertion.reset_expr = disable_expr
            disable_iff = [
                S("defun"),
                S("check-disable"),
                [],
                [
                    S("unless"),
                    S("was-disabled?"),
                    [Op.SET, [S("was-disabled?"), assertion.reset_expr]],
                ],
            ]
            prop_check = [Op.SET, [S("property-status"), prop_expr]]
            assertion.expr = [
                [
                    S("timeframe"),
                    prop_check,
                    Fail(
                        wrapping=[
                            S("unless"),
                            [Op.OR, S("property-status"), S("was-disabled?")],
                        ]
                    ),
                ]
            ]

        if disable_iff:
            assertion.expr = [disable_iff] + assertion.expr

        return assertion

    # numbers
    int = lambda self, i: i[0]
    bin_int = lambda self, i: int(i[0][2:], 2)
    std_int = lambda self, i: int(i[0][2:])
    dec_int = lambda self, i: int(i[0][2:], 10)
    hex_int = lambda self, i: int(i[0], 16)
    SIGNED_INT = lambda self, i: int(i.value)
    INT = lambda self, i: int(i.value)
    string = lambda self, s: ast.literal_eval(s[0])

    # clocking_event : "@" expr
    clocking_event = lambda self, s: ("clocking", s[0])
    # disable_iff : "disable " "iff" "(" expr ")"
    disable_iff = lambda self, s: ("disable", s[0])

    # edge_identifier : posedge | negedge | edge
    edge_identifier = lambda self, s: s[0]
    # posedge : "posedge" _WS simple_symbol
    posedge = lambda self, s: [S("rising"), s[0]]
    # negedge : "negedge" _WS simple_symbol
    negedge = lambda self, s: [Op.AND, s[0][Op.NOT, re(s[0], -1)]]
    # edge : "edge" _WS simple_symbol
    edge = lambda self, s: [Op.NEQ, re(s[0], -1), s[0]]

    sva_op = lambda self, s: s[0]
    op_rose = lambda self, s: [Op.AND, [Op.REL_EVAL, [Op.NOT, s[0]], -1], s[0]]
    op_fell = lambda self, s: [Op.AND, [Op.REL_EVAL, s[0], -1], [Op.NOT, s[0]]]
    op_stable = lambda self, s: [Op.EQ, [Op.REL_EVAL, s[0], -1], s[0]]
    op_signed = lambda self, s: [S("signed"), s[0]]

    def op_past(self, s):
        if s[1]:
            return re(s[0], s[1])
        else:
            return re(s[0], -1)

    def op_wal(self, s):
        fname = s[0].name
        func = Op(fname) if fname in operators else s[0]
        # if some arguments where passed
        if s[1:][0]:
            return [func] + s[1:]
        else:
            return [func]

    def step(self, steps=1):
        return [[Op.STEP, steps]]

    sva_sequence = lambda self, s: s[0]
    seq_delay_un = lambda self, s: [Op.DO, [Op.STEP, s[0]], [S("check-disable")], s[1]]
    seq_delay_single = lambda self, s: [
        Op.AND,
        s[0],
        [Op.DO, *self.step(s[1]), [S("check-disable")], s[2]],
    ]

    def seq_delay_range(self, s):
        start = s[1]
        end = s[2]
        xs = [Op.OR] + [re(s[3], i) for i in range(start, end)]
        return [Op.AND, s[0], xs, [Op.DO, *self.step(end), [S("check-disable")], True]]

    def seq_repetition_once(self, s):
        end = s[1] - 1
        return [Op.AND, s[0]] + [
            [Op.DO, *self.step(), [S("check-disable")], s[0]] for i in range(0, end)
        ]

    def seq_goto_once(self, s):
        expr = s[0]
        reps = s[1]
        return [
            Op.LET,
            [[S("m"), 0]],
            [
                Op.WHILE,
                [
                    Op.AND,
                    [Op.SMALLER, S("INDEX"), S("MAX-INDEX")],
                    [Op.SMALLER, S("m"), reps],
                ],
                [S("when"), expr, [Op.SET, [S("m"), [Op.ADD, S("m"), 1]]]],
                *self.step(),
                [S("check-disable")],
            ],
            [Op.OR, [Op.EQ, S("m"), reps], [Op.EQ, S("INDEX"), S("MAX-INDEX")]],
        ]

    def seq_var_assignment(self, s):
        return [
            Op.LET,
            [],
            *s[1:],
            [Op.QUOTE, s[0]]
        ]

    def sva_var_assignment(self, s):
        return [Op.SET, [s[0], s[2]]]

    def implication(self, x):
        antecedent = x[0]
        op = x[1]
        consequent = x[2]

        if op == "|->":
            return [S("cond"), [antecedent, consequent], [True, True]]
        elif op == "|=>":
            return [
                S("cond"),
                [antecedent, [Op.DO, *self.step(), [S("check-disable")], consequent]],
                [True, True],
            ]
        else:
            raise ParseError()

    implication_op = lambda self, x: x[0]
    mul = lambda self, x: x[0]
    a_mul = lambda self, x: [Op(x[1].children[0].value), x[0], x[2]]
    sum_s = lambda self, x: x[0]
    a_sum_s = lambda self, x: [Op(x[1].children[0].value), x[0], x[2]]
    and_s = lambda self, x: x[0]
    a_and_s = lambda self, x: [Op(x[1].children[0].value), x[0], x[2]]
    or_s = lambda self, x: x[0]
    a_or_s = lambda self, x: [Op(x[1].children[0].value), x[0], x[2]]
    comp = lambda self, x: x[0]
    negation = lambda self, x: [x[0], x[1]] if x[0] else x[1]
    and_op = lambda self, x: x[0]
    or_op = lambda self, x: x[0]

    def a_comp(self, x):
        op = x[1].children[0].value
        op = Op.EQ if op == "==" else Op(op)
        return [op, x[0], x[2]]

    null = lambda self, _: None
    true = lambda self, _: True
    false = lambda self, _: False


parser = Lark(WSVA_GRAMMAR, start="assertions", parser="lalr", debug=False)


def assertions_to_wal_functions(assertions):
    return [expr for assertion in assertions for expr in assertion.emit()]


def assertions_to_wal(assertions):
    functions = assertions_to_wal_functions(assertions)
    return (
        functions
        + [[a.label] for a in assertions]
        + [[Op.IMPORT, S("json")], [Op.CALL, S("json.dumps"), S("status")]]
    )


def parse_sva(code, signals, start="assertions"):
    code, extra_wal_exprs = preprocess(code)

    code_without_comments = ""
    for line in code.split("\n"):
        comment = line.find("//")

        if comment != -1:
            code_without_comments += line[:comment] + "\n"
        else:
            code_without_comments += line + "\n"

    parsed = parser.parse(code_without_comments)
    tree = TreeToWal(signals)
    stmts = tree.transform(parsed)

    prologue = [read_wal_sexpr(expr) for expr in extra_wal_exprs]
    assertions = []
    declarations = []
    for stmt in stmts:
        if isinstance(stmt, Definition):
            prologue.append(stmt.emit())
        elif isinstance(stmt, Assertion):
            assertions.append(stmt)

    res = {
        "assertions": assertions,
        "declarations": declarations,
        "prologue": prologue,
        "clocking_groups": tree.clocking_groups,
    }

    return res


def to_wal(code, start="expr"):
    print(f"{code:20}", end=" = ")
    sexpr = parse_sva(code, start)
    print(wal_str(sexpr))
