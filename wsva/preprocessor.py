import re


def linecontinuations(code):
    out = ""
    last = ""
    comment = False
    continuations = 0
    for c in code:
        if comment and c != "\n":
            pass
        else:
            if c == "/" and last == "/":
                comment = True
                if not comment:
                    pass
            elif c == "\n" and last == "\\":
                continuations += 1
                c = ""
            elif c == "\n":
                if not comment:
                    out += last
                comment = False
                c = c + ("\n" * continuations)
                continuations = 0
            else:
                if not comment:
                    out += last

        last = c

    return out


def macros(code):
    lines = code.split("\n")
    wal_exprs = []
    definitions = {}

    is_wal_re = re.compile(r"^\s*`wal\s+(.*)$")
    is_definition_re = re.compile(r"^\s*`define\s*([_a-zA-Z][\w_]*)\s+(.*)$")
    param_def_re = "^\s*`define\s*([_a-zA-Z][\w_]*)(\(\s*\w*\s*(,\s*\w+\s*)*\))\s+(.*)$"
    is_param_definition_re = re.compile(param_def_re)

    out = ""

    for line in lines:
        # definition substitution
        for id, definition in definitions.items():
            if definition["args"]:
                match = re.search(rf"`{id}(\(\s*[^,]+\s*(,\s*[^,]+\s*)*\))", line)
                expr = definition["expr"]
                if match:
                    args = match.group(1)[1:-1].split(",")
                    args = [arg.strip() for arg in args]
                    assert len(args) == len(
                        definition["args"]
                    ), f"Wrong number of arguments to {id} macro"
                    args = zip(definition["args"], args)
                    for arg in args:
                        expr = expr.replace(arg[0], arg[1])

                    line = expr
            else:
                line = re.sub(rf"`{id}", definition["expr"], line)

        is_definition = is_definition_re.search(line)

        if is_definition:
            id = is_definition.group(1)
            expr = is_definition.group(2)
            definitions[id] = {"args": [], "expr": expr}

            out += "\n"
            continue

        is_param_definition = is_param_definition_re.search(line)
        if is_param_definition:
            id = is_param_definition.group(1)
            args = is_param_definition.group(2)[1:-1].split(",")
            args = [arg.strip() for arg in args]
            expr = is_param_definition.group(4)
            definitions[id] = {"args": args, "expr": expr}
            out += "\n"
            continue

        is_wal = is_wal_re.search(line)
        if is_wal:
            wal_exprs.append(is_wal.group(1))
            out += "\n"
            continue

        out += line + "\n"

    return out, wal_exprs


def preprocess(code):
    return macros(linecontinuations(code))
