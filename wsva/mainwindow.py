import time
from PySide6.QtWidgets import QApplication, QWidget, QMainWindow, QFileDialog
from PySide6.QtWidgets import QTreeWidgetItem, QTableWidgetItem
from PySide6.QtCore import QByteArray
from PySide6.QtGui import QKeySequence
from PySide6.QtCore import Qt

from multiprocessing import Pool, cpu_count, Manager

from wsva.ui.mainwindow import Ui_MainWindow
from wsva.ui.mainform import Ui_Form
from wsva.types import SingleTimeFunction

from wsva.parser import parse_sva, assertions_to_wal_functions
from wsva.waveformviewer import open_surfer
from wal.core import Wal
from wal.ast_defs import Operator as Op


def check_property(data):
    function = data["function"]
    clocking_groups = data["clocking"]
    trace = data["trace"]
    prologue = data["prologue"]
    queue = data["queue"]

    wal = Wal()
    wal.eval_context.global_environment.write("args", [])
    wal.load(trace)

    # evaluate prologue
    for stmt in prologue:
        wal.eval(stmt)

    wal.eval([Op.DO, *[[Op.DEFINE] + x for x in clocking_groups]])
    wal.eval(function)
    label = function[1]
    res = {"label": label.name, "fails": wal.eval([label], stop_on_fail=True)}

    queue.put(res)

    return res


class MainWindow(QMainWindow):
    def __init__(self, args):
        super(MainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        w = QWidget()
        self.setCentralWidget(w)
        self.content = Ui_Form()
        self.content.setupUi(w)

        # file menu
        # open trace
        self.ui.actionOpen_Trace.triggered.connect(self.actionOpen_Trace)
        # open sva
        self.ui.actionOpen_SVA.triggered.connect(self.actionOpen_SVA)
        # save sva
        self.ui.actionSave.triggered.connect(self.actionSave)
        self.ui.actionSave.setShortcut(QKeySequence.Save)
        # save sva as
        self.ui.actionSave_As.triggered.connect(self.actionSave_As)
        # exit
        self.ui.actionExit.triggered.connect(QApplication.quit)
        self.ui.actionExit.setShortcut(QKeySequence("Ctrl+q"))

        # run menu
        self.ui.actionRun_All.triggered.connect(self.actionRun_All)
        self.ui.actionRun_All.setShortcut(QKeySequence.Refresh)

        # Shortcuts
        self.content.progressWidget.hide()

        if args.trace:
            self.actionOpen_Trace(args.trace)

        if args.sv:
            self.actionOpen_SVA(args.sv)

    def actionOpen_Trace(self, trace):
        if trace:
            self.trace = trace
        else:
            fd = QFileDialog.getOpenFileName(
                self, caption="Open Trace", filter="Trace Files (*.vcd *.fst)"
            )
            self.trace = fd[0]

        self.wal = Wal()
        self.wal.load(self.trace)

        # populate design info table
        self.content.traceStats.setRowCount(3)
        self.content.traceStats.setColumnCount(2)
        # Trace
        self.content.traceStats.setItem(0, 0, QTableWidgetItem("Trace"))
        self.content.traceStats.setItem(0, 1, QTableWidgetItem(self.trace))
        # Time
        self.content.traceStats.setItem(1, 0, QTableWidgetItem("Time"))
        self.content.traceStats.setItem(
            1, 1, QTableWidgetItem(str(self.wal.eval_str("TS@MAX-INDEX")))
        )
        # Indices
        self.content.traceStats.setItem(2, 0, QTableWidgetItem("Indices"))
        self.content.traceStats.setItem(
            2, 1, QTableWidgetItem(str(self.wal.eval_str("MAX-INDEX")))
        )
        self.content.traceStats.show()

        # populate hierarchy tree
        get_signals = """
        (do
          (defun scope-signals [scope]
            (in-scope scope (list scope
                                  LOCAL-SIGNALS
                                  (map scope-signals LOCAL-SCOPES))))
          (scope-signals ""))
        """
        hierarchy = self.wal.eval_str(get_signals)

        items = []

        def to_tree(hierarchy):
            name = hierarchy[0]
            signals = hierarchy[1]
            scopes = hierarchy[2]

            if name == "":
                for signal in signals:
                    items.append(QTreeWidgetItem([signal]))
            else:
                scope = QTreeWidgetItem([name])
                items.append(scope)
                for signal in signals:
                    signal_name = signal.split(".")[-1]
                    scope.addChild(QTreeWidgetItem([signal_name]))

            for sub in scopes:
                to_tree(sub)

        to_tree(hierarchy)
        self.content.designHierarchy.clear()
        self.content.designHierarchy.insertTopLevelItems(0, items)
        self.content.designHierarchy.expandToDepth(2)
        self.content.designHierarchy.show()

    def actionOpen_SVA(self, sva_file):
        if sva_file:
            self.sva_file = sva_file
        else:
            fd = QFileDialog.getOpenFileName(
                self, caption="Open SVA File", filter="SystemVerilog Files (*.sv *.txt)"
            )
            self.sva_file = fd[0]

        with open(self.sva_file) as f:
            self.content.sva_editor.clear()
            self.content.sva_editor.insertPlainText(f.read())

    def actionSave(self):
        with open(self.sva_file, "r+") as f:
            f.seek(0)
            f.write(self.content.sva_editor.toPlainText())
            f.truncate()

    def actionSave_As(self):
        data = QByteArray(self.content.sva_editor.toPlainText())
        QFileDialog.saveFileContent(data)

    def actionRun_All(self):
        cpus = cpu_count() - 1
        self.content.verificationResults.clear()
        self.content.progressLabel.setText(f"Verification Running on {cpus} threads)")
        self.content.progressWidget.show()
        self.content.progressWidget.update()
        sva_txt = self.content.sva_editor.toPlainText()
        parsed = parse_sva(sva_txt, signals=self.wal.eval_str("SIGNALS"))
        assertions = parsed["assertions"]

        self.assertions = {}
        for assertion in assertions:
            self.assertions[assertion.label.name] = assertion

        clocking_groups = [
            [grp[0], grp[1]] for grp in parsed["clocking_groups"].values()
        ]

        manager = Manager()
        result_queue = manager.Queue()
        data = []

        assertions = assertions_to_wal_functions(assertions)

        # find single_time_functions
        single_time_functions = {}
        for function in assertions:
            if isinstance(function[1], SingleTimeFunction):
                single_time_functions[function[1].name] = function

        for function in assertions:
            # skip the single time functions
            if isinstance(function[1], SingleTimeFunction):
                continue

            single_time_function = single_time_functions[
                "sva_" + function[1].name + "_single"
            ]
            data.append(
                {
                    "function": function,
                    "clocking": clocking_groups,
                    "prologue": parsed["prologue"]
                    + [single_time_function],  # plug in the correct stf
                    "trace": self.trace,
                    "queue": result_queue,
                }
            )

        status = {}
        n_properties = len(data)

        with Pool(cpus) as p:
            res = p.map_async(check_property, data)

            while not res.ready():
                progress = int(len(status) / n_properties * 100)
                while result_queue.qsize() > 0:
                    property_res = result_queue.get()
                    property_label = property_res["label"]
                    status[property_label] = property_res
                    self.populate_verificationResults(status)

                self.content.verificationProgress.setValue(progress)
                time.sleep(0.1)

        self.content.progressWidget.hide()

        for property_res in res.get():
            property_label = property_res["label"]
            if property_label not in status:
                status[property_label] = property_res

        self.populate_verificationResults(status)

    def populate_verificationResults(self, results):
        class FailingTimeWidget(QTreeWidgetItem):
            def __init__(self, data, label, time):
                super().__init__(data)
                self.label = label
                self.time = time

        items = []
        for label, data in results.items():
            item = QTreeWidgetItem([label])
            items.append(item)

            if data["fails"]:
                item.setForeground(0, Qt.red)
                failing_times = QTreeWidgetItem(["Fails at"])
                item.addChild(failing_times)
                for failing in data["fails"]:
                    time = FailingTimeWidget(
                        [f"{failing[0]} -> {failing[1]}"], label, time=failing
                    )
                    time.label = label
                    failing_times.addChild(time)
            else:
                item.setForeground(0, Qt.green)
                pass

        def opengtkwave(it, col):
            if isinstance(it, FailingTimeWidget):
                try:
                    label = it.label
                    signals = self.assertions[label].get_signals()
                    print("open", it.time)

                    open_surfer(self.wal, self.trace, label, signals, it.time)
                except Exception as e:
                    print(e)
                    pass

        self.content.verificationResults.itemClicked.connect(opengtkwave)
        self.content.verificationResults.clear()
        self.content.verificationResults.insertTopLevelItems(0, items)
        self.content.verificationResults.expandAll()
        self.content.verificationResults.show()
