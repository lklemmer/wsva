// first define property with local variables, then use it later
property check_locals_prop;
  // local variables need an enclosing scope
  // currently just supports 'logic' and 'int' as data types
  // int defaults to 0
  int x, y = 3;
  // logic defaults to 0 (false)
  // x == 0 -> false
  // x != 0 -> true
  logic t = 1, f;
  int a = 42;
  logic b;

  @(posedge tb.clk)
  // variables values are changed left to right
  // currently just supports '=' for assignments
  (x != y && !b && (t, b = t) && b)
endproperty

// use declared property
// if declared property is not used it will not be checked
// check_locals: assert property(check_locals_prop);


property check_inc_prop;
  int x;
  int maxValue = 3;

  @(posedge tb.clk)
  disable iff (tb.value == 0)
  // use x to store the value after overflow
  ((tb.value != 0, x = $past(tb.value)) |-> (tb.value == x + 1 && x < maxValue))
endproperty

check_inc: assert property(check_inc_prop);


property check_overflow_prop;
  logic x;
  int maxValue = 3;

  @(posedge tb.clk)
  disable iff (tb.value != 0)
  // skip initial 0s by checking past values
  ((tb.value == 0, x = $past(tb.value) == 3) |-> ($past(tb.value) == 0 || x))
endproperty

check_overflow: assert property(check_overflow_prop);
