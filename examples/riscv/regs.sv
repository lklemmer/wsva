`define OP_LUI 'b0110111
`define OP_AUIPC 'b0010111
`define OP_ARITH 'b0010011
`define OP_LOAD 'b0000011
`define OP_STORE 'b0100011
`define OP_JAL 'b1101111

`define F7_BRANCH 'b1100011

`define F3_BEQ 'b000
`define F3_BNE 'b001
`define F3_BLTS 'b100
`define F3_BGES 'b101
`define F3_BLTU 'b110
`define F3_BGEU 'b111

`define reg_update_on_write(REGN) \
reg_REGN_update_on_write: assert property( \
  @(posedge testbench.clk) \
  disable iff(testbench.reset) \
  (testbench.dut.rvsingle.dp.rf.we3 && (testbench.dut.rvsingle.dp.rf.a3 == REGN)) \
  |=> (testbench.dut.rvsingle.dp.rf.xREGN == $past(testbench.dut.rvsingle.dp.rf.wd3)) \
);

`define reg_no_change_on_other_write(REGN) \
reg_REGN_no_change_on_other_write: assert property( \
  @(posedge testbench.clk) \
  disable iff(testbench.reset) \
  (testbench.dut.rvsingle.dp.rf.we3 && (testbench.dut.rvsingle.dp.rf.a3 != REGN)) \
   |=> $stable(testbench.dut.rvsingle.dp.rf.xREGN) \
);

// register checks
reg_0_is_always_0: assert property(
  testbench.dut.rvsingle.dp.rf.x0 == 0
);

`reg_update_on_write(1)
`reg_no_change_on_other_write(1)
`reg_update_on_write(2)
`reg_no_change_on_other_write(2)
`reg_update_on_write(3)
`reg_no_change_on_other_write(3)
`reg_update_on_write(4)
`reg_no_change_on_other_write(4)
`reg_update_on_write(5)
`reg_no_change_on_other_write(5)
`reg_update_on_write(6)
`reg_no_change_on_other_write(6)
`reg_update_on_write(7)
`reg_no_change_on_other_write(7)
`reg_update_on_write(8)
`reg_no_change_on_other_write(8)
`reg_update_on_write(9)
`reg_no_change_on_other_write(9)
`reg_update_on_write(10)
`reg_no_change_on_other_write(10)
`reg_update_on_write(11)
`reg_no_change_on_other_write(11)
`reg_update_on_write(12)
`reg_no_change_on_other_write(12)
`reg_update_on_write(13)
`reg_no_change_on_other_write(13)
`reg_update_on_write(14)
`reg_no_change_on_other_write(14)
`reg_update_on_write(15)
`reg_no_change_on_other_write(15)
`reg_update_on_write(16)
`reg_no_change_on_other_write(16)
`reg_update_on_write(17)
`reg_no_change_on_other_write(17)
`reg_update_on_write(18)
`reg_no_change_on_other_write(18)
`reg_update_on_write(19)
`reg_no_change_on_other_write(19)
`reg_update_on_write(20)
`reg_no_change_on_other_write(20)
`reg_update_on_write(21)
`reg_no_change_on_other_write(21)
`reg_update_on_write(22)
`reg_no_change_on_other_write(22)
`reg_update_on_write(23)
`reg_no_change_on_other_write(23)
`reg_update_on_write(24)
`reg_no_change_on_other_write(24)
`reg_update_on_write(25)
`reg_no_change_on_other_write(25)
`reg_update_on_write(26)
`reg_no_change_on_other_write(26)
`reg_update_on_write(27)
`reg_no_change_on_other_write(27)
`reg_update_on_write(28)
`reg_no_change_on_other_write(28)
`reg_update_on_write(29)
`reg_no_change_on_other_write(29)
`reg_update_on_write(30)
`reg_no_change_on_other_write(30)
`reg_update_on_write(31)
`reg_no_change_on_other_write(31)
