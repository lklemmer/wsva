from pathlib import Path

from wsva.parser import parse_sva, assertions_to_wal_functions
from wal.util import wal_str
from wal.ast_defs import Operator as Op
from wal.ast_defs import Symbol as S


def compiler_run(args):
    assertions = {}

    # parse sva
    with open(args.sva_file) as svaf:
        code_txt = svaf.read()
        parsed = parse_sva(code_txt, signals=[])
        assertions = parsed["assertions"]
        clocking_groups = [
            [grp[0], grp[1]] for grp in parsed["clocking_groups"].values()
        ]
        code = [
            [Op.DEFINE, S("stop_on_fail"), False],
            *[[Op.DEFINE] + x for x in clocking_groups],
            *assertions_to_wal_functions(assertions),
        ]

        if args.outfile:
            outfile = args.outfile
        else:
            outfile = Path(args.sva_file).stem + ".wal"

        with open(outfile, "w") as f:
            for expr in parsed["prologue"]:
                f.write(wal_str(expr))

            for expr in code:
                f.write(wal_str(expr))
